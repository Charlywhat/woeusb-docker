FROM ubuntu:23.10
WORKDIR /app
COPY . .
RUN apt update && apt install -y coreutils util-linux grub2 grep gawk parted wget dosfstools ntfs-3g wimtools fdisk
RUN chmod +x woeusb.bash