# WoeUSB Docker

This repo  contains a Dockerfile containing the necessary libraries to run WoeUSB.
It uses https://github.com/WoeUSB/WoeUSB/ (v5.2.4)

# Requirements

- Docker
- A licensed ISO of Windows OS.

# How to run

1. Copy your licensed ISO of Windows OS into this directory.
2. Build the image
```
docker build -t woeusb .
```

3. Run the container
```
docker run --rm --privileged -it woeusb
```

4. You should now be able to run WoeUSB
```
./woeusb.bash --help
```

To exit just type `exit` and hit Enter.

# Troubleshooting

This repo just wraps WoeUSB tool within a Docker image.
Please refer to https://github.com/WoeUSB/WoeUSB/ for more deatils about using WoeUSB.

**PLEASE NOTE: This requires you to run a Docker container in Privileged mode, which may compromise your system's security. Use this at your own risk.**